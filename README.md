# Packer templates to build VM images for various platforms

## Things to remember

* Paths are relative to repo root.
* Never put environment specific info into main branch.
* Environment specific info like which vcenter, folder and such should be in environment branches.
* Packer templates are organized into OS/builder-platform, like centos7/vsphere-iso for example.
* The goal is to never configure anything in the builder specific hcl file, just use different definitions files with variables referenced from the builder config.

## Setup packer

### Install required packer plugins.

    packer init plugins.pkr.hcl

### Upgrade required packer plugins

    packer init -upgrade plugins.pkr.hcl

# How to run

## Configure environment

### Example environment file in repo root

Copy the file ``example-env.pkrvars.hcl`` to your own environment name and edit it.

## Run packer build

    packer build -var-file myenv-staging.pkrvars.hcl centos7/

# Credit

This work is based on the work of others.

* [Packer.io vSphere ISO builder plugin docs](https://www.packer.io/plugins/builders/vsphere/vsphere-iso)
* [github.com/swetakum/packer-ova](https://github.com/swetakum/packer-ova)
* [github.com/linuxtek-canada/packer](https://github.com/linuxtek-canada/packer)
* [github.com/stardata/packer-centos7-kvm-example](https://github.com/stardata/packer-centos7-kvm-example)
* [github.com/hashicorp/packer/issues/10159](https://github.com/hashicorp/packer/issues/10159)
