# More general definitions that cannot identify the environment we're using.
# All other definitions that are specific to the environment should go into
# environment specific files like /env-staging.pkrvars.hcl in the root.

http_directory = "http"
vm_name = "centos7-base-template"
vm_guest_os_family = "linux"
vm_firmware = "bios"
vm_cdrom_type = "sata"
vm_cpu_sockets = 1
vm_cpu_cores = 2
vm_mem_size = 2048
vm_disk_size = 102400
vm_disk_controller_type = ["pvscsi"]
vm_network_card = "vmxnet3"
vm_boot_wait = "4s"
vm_ip_wait_timeout = "10m"

# https://docs.vmware.com/en/VMware-vSphere/6.0/com.vmware.vsphere.vm_admin.doc/GUID-789C3913-1053-4850-A0F0-E29C3D32B6DA.html
vm_version = 14

# https://vdc-repo.vmware.com/vmwb-repository/dcr-public/da47f910-60ac-438b-8b9b-6122f4d14524/16b7274a-bf8b-4b4c-a05e-746f2aa93c8c/doc/vim.vm.GuestOsDescriptor.GuestOsIdentifier.html
#vm_guest_os_type = "centosGuest"
vm_guest_os_type = "centos7_64Guest"

# Not required for vsphere builder
#iso_url = "https://ftp.ember.se/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-2009.iso"
iso_checksum = "sha256:07b94e6b1a0b0260b94c83d6bb76b26bf7a310dc78d7a9c7432809fb9bc6194a"

boot_command = ["<tab> text ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg<enter><wait>"]
