source "vsphere-iso" "centos7-base" {
  vcenter_server = var.vcenter_server
  username = var.vcenter_username
  password = var.vcenter_password
  insecure_connection = var.vcenter_insecure_connection
  datacenter = var.vcenter_datacenter
  datastore = var.vcenter_datastore

  folder = var.vcenter_folder
  #host = var.vcenter_host
  cluster = var.vcenter_cluster

  vm_name = var.vm_name
  vm_version = var.vm_version
  tools_upgrade_policy = true
  remove_cdrom = true
  convert_to_template = true
  notes = "Automated VM template build by packer"
  firmware = var.vm_firmware
  cdrom_type = var.vm_cdrom_type

  iso_paths      = ["[${var.vcenter_datastore}] /${var.iso_path}/${var.iso_file}"]
  iso_checksum   = var.iso_checksum

  guest_os_type = var.vm_guest_os_type
  CPUs = var.vm_cpu_sockets
  cpu_cores = var.vm_cpu_cores
  RAM = var.vm_mem_size
  disk_controller_type = var.vm_disk_controller_type

  http_directory = var.http_directory
  boot_order = "disk,cdrom"
  boot_wait = var.vm_boot_wait
  boot_command = var.boot_command

  storage {
    disk_size = var.vm_disk_size
    disk_thin_provisioned = true
  }

  network_adapters {
    network_card = var.vm_network_card
    network = var.vcenter_network
  }

  ssh_username = var.ssh_username
  ssh_password = var.ssh_password
}

build {
  sources = ["source.vsphere-iso.centos7-base"]
}
